local plug_list = function(use)
  -- Core plugins, that are needed by other ones
  use "wbthomason/packer.nvim" -- Have packer manage itself
  use "nvim-lua/popup.nvim" -- An implementation of the Popup API from vim in Neovim
  use "nvim-lua/plenary.nvim" -- Useful lua functions used in lots of plugins
  use "lewis6991/impatient.nvim"
  use "moll/vim-bbye" -- Do not kill Neovim, when closing the buffer
  use "antoinemadec/FixCursorHold.nvim" -- This is needed to fix lsp doc highlight
  use "tpope/vim-repeat" -- Repeat things
  use "MunifTanjim/nui.nvim"

  -- UI
  use "akinsho/bufferline.nvim" -- Tabs on top
  use "akinsho/toggleterm.nvim" -- Float terminal
  use "catppuccin/nvim" -- Color scheme
  use "folke/which-key.nvim" -- Essential for key discoverability
  use "goolord/alpha-nvim" -- Dashboard
  use "kevinhwang91/nvim-bqf" -- Better quickfix list
  use "kyazdani42/nvim-tree.lua" -- Tree with operations
  use "kyazdani42/nvim-web-devicons" -- Icons for various parts
  use "lukas-reineke/indent-blankline.nvim" -- Indent guides
  use "numToStr/Comment.nvim" -- Easily comment stuff
  use "nvim-lualine/lualine.nvim" -- Bottom status line
  use "nvim-neotest/neotest" -- Run Unit Tests

  -- Navigation enhancements
  use "wellle/targets.vim" -- Better text objects
  use "andymass/vim-matchup" -- Better %
  use "ggandor/lightspeed.nvim" -- Fast navigation using s
  use "ThePrimeagen/harpoon" -- Project marks on steroids

  -- Completion
  use "windwp/nvim-autopairs" -- Autopairs, integrates with both cmp and treesitter
  use "hrsh7th/nvim-cmp" -- The completion plugin
  use "hrsh7th/cmp-buffer" -- buffer completions
  use "hrsh7th/cmp-path" -- path completions
  use "hrsh7th/cmp-cmdline" -- cmdline completions
  use "hrsh7th/cmp-nvim-lsp"
  use "saadparwaiz1/cmp_luasnip" -- snippet completions
  use { -- AI completion
    "tzachar/cmp-tabnine",
    run = "./install.sh",
  }

  -- Snippets
  use "L3MON4D3/LuaSnip" --snippet engine
  use "rafamadriz/friendly-snippets" -- a bunch of snippets to use

  -- LSP
  use "neovim/nvim-lspconfig" -- enable LSP
  use "williamboman/nvim-lsp-installer" -- simple to use language server installer
  use "jose-elias-alvarez/null-ls.nvim" -- for formatters and linters
  use "ray-x/lsp_signature.nvim"
  use { "weilbith/nvim-code-action-menu", cmd = "CodeActionMenu" } -- Code action fancy menu
  use "b0o/schemastore.nvim" -- JSON schemas from the schema-store
  use "simrat39/rust-tools.nvim" -- Rust Extras
  use "p00f/clangd_extensions.nvim" -- C/C++ Extras
  use "kosayoda/nvim-lightbulb" -- Discoverable code actions

  -- Debugging
  use "mfussenegger/nvim-dap" -- Core
  use "nvim-telescope/telescope-dap.nvim" -- Search for stuff
  use "rcarriga/nvim-dap-ui" -- UI
  use "theHamsta/nvim-dap-virtual-text" -- Virtual text variables

  -- Telescope
  use "nvim-telescope/telescope.nvim"
  use "ahmedkhalf/project.nvim"

  -- Treesitter & Highlighting
  use { "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" }
  use "nvim-treesitter/playground"
  use "JoosepAlviste/nvim-ts-context-commentstring"
  use "folke/todo-comments.nvim" -- Heighlight todo comments

  -- Git
  use "lewis6991/gitsigns.nvim" -- Git changes near the numbers

  -- Chezmoi file detection
  use "alker0/chezmoi.vim"

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end

local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
local packer_user_config = vim.api.nvim_create_augroup("packer_user_config", {})
vim.api.nvim_create_autocmd("BufWritePost", {
  group = packer_user_config,
  pattern = "plugins.lua",
  command = "source <afile> | PackerSync",
})

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

-- Have packer use a popup window
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
}

-- Install your plugins here
return packer.startup(plug_list)
