local augroup = function(groupName)
  return vim.api.nvim_create_augroup(groupName, {})
end

local autocmd = vim.api.nvim_create_autocmd

local general = augroup "_general_settings"

autocmd("FileType", {
  group = general,
  desc = "Close some buffers quickly",
  pattern = { "qf", "help", "man", "lspinfo" },
  command = "nnoremap <silent> <buffer> q :close<CR>",
})

autocmd("TextYankPost", {
  group = general,
  desc = "Highlight yanked text",
  callback = function()
    require("vim.highlight").on_yank {
      higroup = "Visual",
      timeout = 200,
    }
  end,
})

autocmd("BufWinEnter", {
  group = general,
  command = "set formatoptions-=cro",
})

autocmd("FileType", {
  group = general,
  desc = "Exclude quickfix list from the buffer list",
  pattern = "qf",
  command = "set nobuflisted",
})

autocmd("FileType", {
  desc = "Disable spelling for non-editable buffers",
  group = general,
  pattern = {
    "qf",
    "help",
    "dapui_breakpoints",
    "dapui_scopes",
    "dapui_stacks",
    "dapui_watches",
    "dap-repl",
  },
  command = "setlocal nospell",
})

local git = augroup "_git"

autocmd("FileType", {
  group = git,
  desc = "Force wrap in gitcommit",
  command = "setlocal wrap",
  pattern = { "gitcommit", "markdown" },
})

autocmd("FileType", {
  group = git,
  desc = "Force spelling in gitcommit",
  command = "setlocal spell",
  pattern = { "gitcommit", "markdown" },
})

local autoResize = augroup "_auto_resize"

autocmd("VimResized", {
  group = autoResize,
  desc = "Automatic resize",
  command = "tabdo wincmd = ",
})

local autoReload = augroup "_auto_reload"

autocmd({ "FocusGained", "BufEnter", "CursorHold", "CursorHoldI" }, {
  group = autoReload,
  desc = "Check time periodically",
  command = "if mode() != 'c' | checktime | endif",
})

autocmd("FileChangedShellPost", {
  group = autoReload,
  desc = "Show warning when file is reloading",
  command = 'echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None',
})
