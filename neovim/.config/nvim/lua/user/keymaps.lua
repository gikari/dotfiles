-- Remap space as leader key
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Normal Mode --

-- Better window navigation
vim.keymap.set("n", "<C-h>", "<C-w>h", { silent = true, desc = "Focus Window on the Left" })
vim.keymap.set("n", "<C-j>", "<C-w>j", { silent = true, desc = "Focus Window on the Bottom" })
vim.keymap.set("n", "<C-k>", "<C-w>k", { silent = true, desc = "Focus Window on the Top" })
vim.keymap.set("n", "<C-l>", "<C-w>l", { silent = true, desc = "Focus Window on the Right" })
vim.keymap.set("n", "<C-Left>", "<C-w>h", { silent = true, desc = "Focus Window on the Left" })
vim.keymap.set("n", "<C-Right>", "<C-w>l", { silent = true, desc = "Focus Window on the Bottom" })
vim.keymap.set("n", "<C-Up>", "<C-w>k", { silent = true, desc = "Focus Window on the Top" })
vim.keymap.set("n", "<C-Down>", "<C-w>j", { silent = true, desc = "Focus Window on the Right" })

-- Common actions
vim.keymap.set("n", "<leader>w", "<cmd>wa!<CR>", { desc = "Save All" })
vim.keymap.set("n", "<leader>c", "<cmd>Bdelete!<CR>", { desc = "Close Buffer" })
vim.keymap.set("n", "<leader>h", "<cmd>nohlsearch<CR>", { desc = "Turn Off Highlight" })
vim.keymap.set("n", "<leader>q", "<cmd>q!<CR>", { desc = "Close Window" })
vim.keymap.set("n", "<leader>e", "<cmd>NvimTreeFindFileToggle<CR>", { desc = "Open File Tree" })

-- Resize with arrows
-- vim.keymap.set("n", "<C-Up>", ":resize -2<CR>", { silent = true, desc = "Shrink Window Height" })
-- vim.keymap.set("n", "<C-Down>", ":resize +2<CR>", { silent = true, desc = "Grow Window Height" })
-- vim.keymap.set("n", "<C-Left>", ":vertical resize -2<CR>", { silent = true, desc = "Shrink Window Width" })
-- vim.keymap.set("n", "<C-Right>", ":vertical resize +2<CR>", { silent = true, desc = "Grow Window Width" })

-- Navigate buffers
vim.keymap.set("n", "gt", ":BufferLineCycleNext<CR>", { silent = true, desc = "Switch to the Next Buffer" })
vim.keymap.set("n", "gT", ":BufferLineCyclePrev<CR>", { silent = true, desc = "Switch to the Previous Buffer" })

-- Insert Mode --
vim.keymap.set("i", "jk", "<ESC>", { desc = "To Normal Mode" })
vim.keymap.set("i", "kj", "<ESC>", { desc = "To Normal Mode" })

-- Visual Mode --

-- Stay in indent mode
vim.keymap.set("v", "<", "<gv", { silent = true, desc = "Decrease Indent" })
vim.keymap.set("v", ">", ">gv", { silent = true, desc = "Increase Indent" })

-- Visual Block Mode --

-- Terminal Mode --
