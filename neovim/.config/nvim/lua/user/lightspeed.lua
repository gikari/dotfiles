local status_ok, lightspeed = pcall(require, "lightspeed")
if not status_ok then
  return
end

local keymap = vim.api.nvim_set_keymap

keymap("n", "s", "<Plug>Lightspeed_omni_s", {})

lightspeed.setup {
  ignore_case = false,
}
