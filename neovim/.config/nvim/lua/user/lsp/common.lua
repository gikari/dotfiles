local M = {}

M.capabilities = function()
  return require("cmp_nvim_lsp").update_capabilities(vim.lsp.protocol.make_client_capabilities())
end

M.add_lsp_keymaps = function(bufnr)
  vim.keymap.set("n", "gD", vim.lsp.buf.declaration, { buffer = bufnr, desc = "Go to Declaration" })
  vim.keymap.set("n", "gd", vim.lsp.buf.definition, { buffer = bufnr, desc = "Go to Definition" })
  vim.keymap.set("n", "K", vim.lsp.buf.hover, { buffer = bufnr, desc = "LSP Hover" })
  vim.keymap.set("n", "gi", vim.lsp.buf.implementation, { buffer = bufnr, desc = "Go to Implementation" })
  vim.keymap.set("n", "<C-k>", vim.lsp.buf.signature_help, { buffer = bufnr, desc = "Signature Help" })
  -- vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, { buffer = bufnr, desc = "Rename Symbol" })
  vim.keymap.set("n", "gr", vim.lsp.buf.references, { buffer = bufnr, desc = "Symbol References" })
  -- vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, { buffer = bufnr, desc = "Code Action" })
  vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, { buffer = bufnr, desc = "Go to Next Diagnostic" })
  vim.keymap.set("n", "gl", vim.diagnostic.open_float, { buffer = bufnr, desc = "Open Diagnostic Float" })
  vim.keymap.set("n", "]d", vim.diagnostic.goto_next, { buffer = bufnr, desc = "Go to Previous Diagnostic" })
  -- vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist, { buffer = bufnr, desc = "Diagnostic to local list" })
end

M.add_current_symbol_highlight = function(client, bufnr)
  -- Set autocommands conditional on server_capabilities
  if client.resolved_capabilities.document_highlight then
    local lsp_document_highlight = vim.api.nvim_create_augroup("lsp_document_highlight", {})

    vim.api.nvim_create_autocmd("CursorHold", {
      group = lsp_document_highlight,
      buffer = bufnr,
      callback = vim.lsp.buf.document_highlight,
    })

    vim.api.nvim_create_autocmd("CursorMoved", {
      group = lsp_document_highlight,
      buffer = bufnr,
      callback = vim.lsp.buf.clear_references,
    })
  end
end

M.add_autocmds = function(bufnr)
  local lsp = vim.api.nvim_create_augroup("_lsp", {})

  vim.api.nvim_create_autocmd("BufWritePre", {
    group = lsp,
    desc = "Automatic formatting on save",
    callback = vim.lsp.buf.formatting_sync,
    buffer = bufnr,
  })

  local lightbulb = require "nvim-lightbulb"

  vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
    desc = "Show lightbulb",
    callback = lightbulb.update_lightbulb,
    buffer = bufnr,
  })
end

M.default_on_attach = function(client, bufnr)
  M.add_lsp_keymaps(bufnr)
  M.add_current_symbol_highlight(client, bufnr)
  M.add_autocmds(bufnr)
  require("lsp_signature").on_attach()
end

return M
