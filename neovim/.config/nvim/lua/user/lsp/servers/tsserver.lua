local common = require "user.lsp.common"

require("lspconfig").tsserver.setup {
  on_attach = function(client, bufnr)
    client.resolved_capabilities.document_formatting = false
    common.default_on_attach(client, bufnr)
  end,
  capabilities = common.capabilities(),
}
