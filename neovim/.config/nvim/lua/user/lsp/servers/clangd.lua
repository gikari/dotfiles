local common = require "user.lsp.common"

require("clangd_extensions").setup {
  server = {
    on_attach = common.default_on_attach,
    capabilities = common.capabilities(),
  },
  extensions = {
    -- Automatically set inlay hints (type hints)
    autoSetHints = true,
  },
}

local cpp = vim.api.nvim_create_augroup("_cpp", {})

vim.api.nvim_create_autocmd("FileType", {
  group = cpp,
  desc = "Add switch source header",
  pattern = { "cpp", "c", "objcpp" },
  command = "nnoremap <buffer> <leader>lh :ClangdSwitchSourceHeader<CR>",
})

vim.api.nvim_create_autocmd("FileType", {
  group = cpp,
  desc = "Force shiftwidth",
  pattern = { "cpp", "c", "objcpp" },
  command = "setlocal shiftwidth=4",
})
