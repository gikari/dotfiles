local common = require "user.lsp.common"

require("rust-tools").setup {
  server = {
    on_attach = common.default_on_attach,
    capabilities = common.capabilities(),
    settings = {
      ["rust-analyzer"] = {
        diagnostics = {
          disabled = { "unresolved-macro-call" },
        },
        procMacro = {
          ignored = {
            ["cpp"] = { "__cpp_internal_closure" },
            ["cpp_macros"] = { "__cpp_internal_closure" },
          },
        },
      },
    },
  },
}
