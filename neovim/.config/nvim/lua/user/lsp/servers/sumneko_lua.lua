local common = require "user.lsp.common"

require("lspconfig").sumneko_lua.setup {
  on_attach = function(client, bufnr)
    client.resolved_capabilities.document_formatting = false
    common.default_on_attach(client, bufnr)
  end,
  capabilities = common.capabilities(),
  settings = {
    Lua = {
      diagnostics = {
        globals = { "vim" },
      },
      workspace = {
        library = {
          [vim.fn.expand "$VIMRUNTIME/lua"] = true,
          [vim.fn.stdpath "config" .. "/lua"] = true,
        },
      },
    },
  },
}
