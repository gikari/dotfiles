local status_ok, lspconfig = pcall(require, "lspconfig")
if not status_ok then
  return
end

local common = require "user.lsp.common"

vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
  border = "rounded",
})

vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
  border = "rounded",
})

local servers = {
  "bashls",
  "clangd",
  "cmake",
  "dockerls",
  "html",
  "jsonls",
  "lemminx",
  "pyright",
  "rust_analyzer",
  "sumneko_lua",
  "tsserver",
  "yamlls",
}

require("nvim-lsp-installer").setup {
  ensure_installed = servers,
}

for _, server in pairs(servers) do
  local import_succeed, _ = pcall(require, "user.lsp.servers." .. server)
  if not import_succeed then
    lspconfig[server].setup {
      on_attach = common.default_on_attach,
      capabilities = common.capabilities(),
    }
  end
end
