local status_ok, alpha = pcall(require, "alpha")
if not status_ok then
  return
end

local dashboard = require "alpha.themes.dashboard"

dashboard.section.header.val = {
  [[                 .oooooooooooooo.]],
  [[              .d888888888888888P]],
  [[     db       88888888888888888boo.]],
  [[    `Y8b      888888888888888888888b.]],
  [[      `Yb....d88888888888888888888888b]],
  [[        `Y8888888888888888888888888888]],
  [["8b  "8" "88888888888888888888888888P']],
  [[ 8Yb  8   88`Y8888888888888bo. """]],
  [[ 8 Yb 8   88-<`Y88888888888888bo.]],
  [[ 8  Yb8   88    `Y888888888888888b]],
  [[.8.  Y8  .88...d `Y88888888888888]],
  [[                    `Y888888888888b]],
  [[             "88""Yb. YY88888888888b]],
  [[              88..bP"  YbY8888888888.]],
  [[              88""Yb    Yb`Y888888888]],
  [[              88   Yb    YdP`Y888888P]],
  [[             .88.   Yb    V   `Y8888b]],
  [[                                `Y888]],
  [[                                  `Y8]],
  [[                                    ']],
}

dashboard.section.buttons.val = {
  dashboard.button("p", "  Open project", ":Telescope projects <CR>"),
  dashboard.button("r", "  Recently used files", ":Telescope oldfiles <CR>"),
  dashboard.button("e", "  New file", ":ene <BAR> startinsert <CR>"),
  dashboard.button("c", "  Configuration", ":e ~/.config/nvim/init.lua <CR>"),
  dashboard.button("q", "  Quit Neovim", ":qa<CR>"),
}

local function footer()
  return {
    "   God’s in His heaven —",
    "All’s right with the world!",
  }
end

dashboard.section.footer.val = footer()

dashboard.section.footer.opts.hl = "Error"
dashboard.section.header.opts.hl = "Error"
dashboard.section.buttons.opts.hl = "Keyword"

dashboard.opts.opts.noautocmd = true

alpha.setup(dashboard.opts)

vim.keymap.set("n", "<leader>a", "<cmd>Alpha<cr>", { desc = "Dashboard" })
